<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

error_reporting(E_ALL);
ini_set('display_errors', 1);
class Login extends MX_Controller {


function __construct()  
	{ 
		parent::__construct();
		$this->load->model('login_model');
		//$this->load->helper('cookie');
		if($this->session->userdata('client_id'))
		{
		   header('location:'.base_url().'client-area');
		  
		}
		
	}	


	public function index()	
	{	
	   $this->load->view('login_view');
		}


 
 	function loginAuth()
	{
		
		$email_address  = strip_tags($this->input->post('email_address'));
		$password		= strip_tags($this->input->post('password'));
		$r		= strip_tags($this->input->post('r'));
		
		if($this->session->userdata('client_id'))
		{
		  echo 1;
		}
		elseif(trim($email_address) == '' || trim($email_address) == null)
		    {
			    echo '-1';
			}
			elseif($this->emailValidate($email_address) == false)
			{
				echo '-2';
			}
			elseif(trim($password) ==  '' || trim($password) == null)
		     {
			    echo '-3';
			 }
			 else
			 {
				$resp = $this->login_model->loginAuth($email_address,md5($password)); 
										$this->load->helper('cookie');

				if($resp == 1 && $r){
			if(!isset($_COOKIE["member_login"])) {
			 $cookie1 = array(
					   'name'   => 'member_login',
					   'value'  => $email_address,
					   'expire' =>time() + (10 * 365 * 24 * 60 * 60));
				   $cookie2 = array(
					   'name'   => 'member_password',
					   'value'  => $password,
					   'expire' => time() + (10 * 365 * 24 * 60 * 60));
			 
						$this->input->set_cookie($cookie1);					
						$this->input->set_cookie($cookie2);					
				
			}      				}
				if($resp == false)
				{
				  echo 0;
				}
				else
				{
					if($r == 'false'){
						//unset($_COOKIE["member_login"]);
			 $cookie1 = array(
					   'name'   => 'member_login',
					   'value'  => '',
					   'expire' =>time() - 3600);
					   unset($_COOKIE['member_login']);
						$this->input->set_cookie($cookie1);
						//delete_cookie('member_password','allmasajid.net');

			 $cookie2 = array(
					   'name'   => 'member_password',
					   'value'  => '',
					   'expire' =>time() - 3600);
					   unset($_COOKIE['member_password']);
						$this->input->set_cookie($cookie2);
						//delete_cookie('member_password','allmasajid.net');

					}
					  echo $resp;	
					
					//'-0';
				}
			 }
	}
	function logout()
	{
		// $this->addLogs('Logout ',$this->session->userdata('avan_user_name'),$this->session->userdata('avan_id'),$this->session->userdata('avan_user_name').' has been logout');
		 $this->session->sess_destroy();
		 header('location:'.base_url());
	}
	
	
		public function emailValidate($email)
		 {
			if(!filter_var($email, FILTER_VALIDATE_EMAIL))
			{
				return 0;
			}
			else
				{
					return 1;
				}
		 }


		 function loginDashboard()
		 {

		 	echo "login successfull";
		 }
		  public function admin_signin()
		  {
			$this->load->view('superadmin/login');
			return true;
		  }




}
