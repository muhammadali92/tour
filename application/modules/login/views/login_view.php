<!DOCTYPE HTML>
<html lang="zxx">

<head>
	<title>Login</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content=""
	/>
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- Meta tag Keywords -->
	<!-- css files -->
	<link rel="stylesheet" href="css/Lstyle.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link rel="stylesheet" href="css/Lfont-awesome.css">
	<!-- Font-Awesome-Icons-CSS -->
	<!-- //css files -->
	<!-- web-fonts -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
	<!-- //web-fonts -->
</head>

<body style="background-image: url(images/page_bg_blur02.jpg);background-size: 100% 100%;">
	<div class="video-w3l" >
		<!--header-->
		<div class="header-w3l">
			<h1>
				
				<span>L</span>ogin
				<span>F</span>orm
			</h1>
		</div>
		<!--//header-->
		<div class="main-content-agile">
			<div class="sub-main-w3">
				<h2>Login Here
					<i class="fa fa-hand-o-down" aria-hidden="true"></i>
				</h2>
				<form action="#" method="post">
					<div class="pom-agile">
						<span class="fa fa-user-o" aria-hidden="true"></span>
						<input placeholder="Username" name="Name" class="user" type="text" required="">
					</div>
					<div class="pom-agile">
						<span class="fa fa-key" aria-hidden="true"></span>
						<input placeholder="Password" name="Password" class="pass" type="password" required="">
					</div>
					<div class="sub-w3l">
						<div class="sub-agile">
							<input type="checkbox" id="brand1" value="">
							<label for="brand1">
								<span></span>Remember me</label>
						</div>
						<a href="#">Forgot Password?</a>
						<div class="clear"></div>
					</div>
					<div class="right-w3l">
						<input type="submit" value="Login">
					</div>
				</form>
			</div>
		</div>
		<!--//main-->
		<!--footer-->
		<div class="footer">
			<p>All rights reserved 
				
			</p>
		</div>
		<!--//footer-->
	</div>

	<!-- js -->
	<script src="js/Ljquery-2.1.4.min.js"></script>
	<script src="js/Ljquery.vide.min.js"></script>
	<!-- //js -->

</body>
 <script>
    
/**
*  Method description       : this method is used for validate login funcationality..
*  Description of parameters  :  -- 
*  Date created             :  26-2-2016
*  Created by                 :  Mohammad Ali.
**/
     

$("input").keypress(function(event) {
    if (event.which == 13) {
          login();
    }
});


     
    function login() {


  // take input field value in variable
  var email_address = $('#email_address').val();
  var password      = $('#password').val();
  var r = $('#rememberme').is(':checked');
  var reDirect ='<?php echo "/".base_url()."home"; ?>';
   //alert(reDirect);
  // alert(userName);
  // alert(userPwd);
  // ajax request for send username and password data
    $.ajax({
                                          url: '<?php echo base_url(); ?>login-auths',  
            type: 'POST',
            dataType: 'HTML',         

            data : "email_address="+email_address+"&password="+password+"&r="+r,
            success: function(res)
            {
              if (res == '-1') { 
                 
                $('#error_msg').html('Email Required');
                $('.error_div').removeClass('hide');
              
              }
              else if (res == '-2') { 
                 
                $('#error_msg').html('Invalid Email');
                $('.error_div').removeClass('hide');
              
              }
              else if (res == '-3') { // if response return 1 then redirect to memberList.php
                 
                $('#error_msg').html('Password Required');
                $('.error_div').removeClass('hide');
              
              }
              else if (res == '0') { // if response return 1 then redirect to memberList.php
                 
                $('#error_msg').html('Invalid Email Or Password');
                $('.error_div').removeClass('hide');
              
              }
              else if (res == 1) { // if response return 1 then redirect to memberList.php
                 
                $('#error_msg').html('');
               
                window.location = "<?php echo base_url();?>client-area";
              }
              else { 
                 
                $('#error_msg').html('Something Went Wrong');
                $('.error_div').removeClass('hide');
              
              }
              
             
              
          
            },
            error: function(xhr, status, error)
            {
              //alert('Error In login');
            }
            });
    }
function forgetPassword()
  {
  	$('#forgetPasswordModal').modal('show');
  }

  function submitForgetPassword()
  {
  	$('#errorDiv').html('');

    $('#errorDiv').css('font-weight','normal');

    var email    =  $('#forgetEmailId').val();

    if (email.length == '')
    {
      $('#errorDiv').css('color','red');
      $('#errorDiv').css('font-weight','bold');
      $('#errorDiv').html('Please Enter Email.');
    }
    else if(validateEmail(email) == false)
      {
        $('#errorDiv').css('color','red');
      $('#errorDiv').css('font-weight','bold');
        $('#errorDiv').html('Invalid Email');
      }
    else
    {
      $.ajax({
        url:'<?php echo base_url();?>send-password-link',
        type:'POST',
        datatype:'HTML',
        data:"email="+email,
        success: function(res)
        {
          var data = $.parseJSON(res);
          if (data.errorCode == 2 && data.success == 0)
          {
            $('#errorDiv').css('color','red');
            $('#errorDiv').css('font-weight','bold');
            $('#errorDiv').html('Email not exist in our system.');
          }
          else if (data.errorCode == 4 && data.success == 0)
          {
            $('#errorDiv').css('color','red');
            $('#errorDiv').css('font-weight','bold');
            $('#errorDiv').html('Invalid Email.');
          }
          else if(data.success == 1 && data.error == 0)
          {
            $('#errorDiv').css('color','#1875bb');
            //$('#errorDiv').css('font-weight','bold');
            $('#errorDiv').html('Reset Password link has been sent. Please check your Email.');
            $('#forgetEmailId').val('');
          }
        },
      })
    }

  }	
  function validateEmail(email) {
      var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      return re.test(email);
  }  
</script>
</html>