
<!DOCTYPE HTML>
<html>
<head>
<title>Tour Register</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Custom Theme files -->
<link href="<?php echo base_url(); ?>assets/css/rstyle.css" rel='stylesheet' type='text/css' />
<!--fonts--> 
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
<!--//fonts--> 
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css" type="text/css" media="all">
	<!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css" type="text/css" media="all">
	<!-- Font-Awesome-Icons-CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/lightbox.css" type="text/css" media="all">
	<link href="<?php echo base_url(); ?>assets/css/easy-responsive-tabs.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css" media="all" />
	<!-- Style-CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/Lfont-awesome.css">
</head>
<body><div class="header-w3">
			
			<div class="header-bottom-agile">
				<div class=" navigation">
					<nav class="navbar navbar-default cl-effect-16" id="cl-effect-16">
					
						<div class="logo-agileinfo">
							<h1><a href="index.html">Register form</a></h1>
						</div>

					
					</nav>
				</div>
	</div>
<!--background-->
<div class="video-w3l" data-vide-bg="video/2">

    <div class="bg-agile">
	<div class="book-appointment">
	<h2>Register Information</h2>
			<form action="#" method="post">
				<div class="left-agileits-w3layouts same">
					<div class="gaps">
						<p>First Name</p>
						<input type="text"  id="firstName" name="First Name" placeholder="" required=""/>
					</div>	
					<div class="gaps">
						<p>Last Name</p>
							<input type="text" id="lastName" name="Last Name" placeholder="" required=""/>
					</div>
					<div class="gaps">
						<p>Email</p>
							<input type="text" id="email" name="Last Name" placeholder="" required=""/>
					</div>
					<div class="gaps">
						<p>Password</p>
							<input type="password" id="password" name="Last Name" placeholder="" required=""/>
					</div>

					<div class="gaps">
						<p>Gender</p>	
							<select  id="gender" class="form-control">
								<option value="0" >Select Gender</option>
								<option>Male</option>
								<option>Female</option>
							</select>
					</div>
					<div class="gaps">
						<p>Select Date</p>		
						<input  id="datepicker1" name="Text" type="text" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'mm/dd/yyyy';}" required="">
					</div>
					<div class="gaps">
						<p>Hospital Preference</p>
						<input type="text" id="hospitalPreference" name="First Name" placeholder="" required=""/>
					</div>	
					<div class="gaps">
						<p>Insurance Company</p>
						<input type="text" id="insuranceCompany" name="First Name" placeholder="" required=""/>
					</div>			
					<div class="gaps">	
						<p>Policy Number</p>
						<input type="text" id="policyNumber" name="Number" placeholder="" required=""/>
					</div>
					<div class="gaps">
						<p>Physician's Name</p>
						<input type="text" id="physicianName" name="First Name" placeholder="" required=""/>
					</div>		
					<div class="gaps">	
						<p>Phone Number</p>
						<input type="text" id="phoneNumber" name="Number" placeholder="" required=""/>
					</div>
				</div>
				<div class="right-agileinfo same">
					<div class="gaps">
						<p>Name</p>
						<input type="text" id="name" name="First Name" placeholder="" required=""/>
					</div>
					<div class="gaps">
						<p>Relationship</p>
						<input type="text"  id="relationship" name="First Name" placeholder="" required=""/>
					</div>
					<div class="gaps">
						<p>Address</p>
						<textarea id="address" name="message" placeholder="" title="Please enter Your Comments"></textarea>
						</div>
					<div class="gaps">
						<p>City</p>
						<input type="text" id="city" name="First Name" placeholder="" required=""/>
					</div>
					
					<div class="gaps">
						<p>State</p>	
						<select  id="states" class="form-control">
							<option value="0" >Select State</option>
							<option>State-1</option>
							<option>State-2</option>
							<option>State-3</option>
							<option>State-4</option>
							<option>State-5</option>
						</select>
					</div>
					<div class="gaps">
						<p>Country</p>	
						<select id="country" class="form-control">
							<option value="0" >Select Country</option>
							<option>Country-1</option>
							<option>Country-2</option>
							<option>Country-3</option>
							<option>Country-4</option>
							<option>Country-5</option>
						</select>
					</div>
					<div class="gaps">	
						<p>Home Phone</p>
						<input  id="homePhone" type="text" name="Number" placeholder="" required=""/>
					</div>
				</div>
				<div class="clear"></div>
				<input type="button"  onclick="registration()" value="Submit">

                 <div  style="display:none;" class=" alert alert-danger errorDiv "> </div>
                <div  style="display:none;" class=" alert alert-success successDiv "> </div>         
			</form>
		</div>
   </div>
   <!--copyright-->
			<div class="copy w3ls">
		       <p>&copy; 2018. All Rights Reserved   </p>
	        </div>
		<!--//copyright-->
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/rjquery-2.1.4.min.js"></script>
		<!-- Calendar -->
				<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/rjquery-ui.css" />
				<script src="<?php echo base_url(); ?>assets/js/rjquery-ui.js"></script>
				  <script>
						  $(function() {
							$( "#datepicker,#datepicker1,#datepicker2,#datepicker3" ).datepicker();
						  });
						  
function registration()
{

    
  var firstName = $('#firstName').val();
  var lastName = $('#lastName').val();  
  var email = $('#email').val();  
  var password = $('#password').val();  
  var gender  = $('#gender').val();
  var datepicker1  = $('#datepicker1').val();
  var hospitalPreference  = $('#hospitalPreference').val();
  var insuranceCompany = $('#insuranceCompany').val();
  var policyNumber = $('#policyNumber').val();
  var physicianName = $('#physicianName').val();
  var phoneNumber = $('#phoneNumber').val();

  var relationship = $('#relationship').val();
  var name = $('#name').val();
  var address = $('#address').val();
  var city = $('#city').val();
  var states = $('#states').val();
  var country = $('#country').val();
  var homePhone = $('#homePhone').val();
  
  $('#firstName').css('border','');
  $('#lastName').css('border','');
  $('#email').css('border','');
  $('#gender').css('border','');
  $('#hospitalPreference').css('border','');
  $('#insuranceCompany').css('border','');
  $('#policyNumber').css('border','');
  $('#physicianName').css('border','');
  $('#phoneNumber').css('border','');
  $('#datepicker1').css('border','');
  $('#name').css('border','');
  $('#relationship').css('border','');
  $('#address').css('border','');
  $('#city').css('border','');
  $('#states').css('border','');
  $('#country').css('border','');
  $('#homePhone').css('border','');

  if($.trim(firstName).length == 0)
   {
      $('#firstName').css('border','1px solid red');
      $('.errorDiv').html(' <strong>Error!</strong> Please enter First Name.');
      $('.errorDiv').fadeIn().delay(5000).fadeOut();
   }
  else if($.trim(lastName).length == 0)
  {
    $('#lastName').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Please enter last Name.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
  }
  else if($.trim(email).length == 0)
  {
    $('#email').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Please enter email address.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
  }
  else if(validateEmail(email) == false)
  {
    $('#email').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Invalid email address.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }  
  else if($.trim(password).length == 0)
  {
    $('#password').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> password required.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }  
  else if($.trim(password).length < 8)
  {
    $('#password').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> password must be atleast 8 chars.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }  
  
  else if(gender == 0)
  {
    $('#gender').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Please select gender.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }
  else if($.trim(datepicker1).length == 0)
  {
    $('#datepicker1').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Please Select DOB.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }
  else if($.trim(hospitalPreference).length == 0)
  {
    $('#hospitalPreference').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Please enter hospital Preference.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }
  else if($.trim(insuranceCompany).length == 0)
  {
    $('#insuranceCompany').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Please enter Insurance Company.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }
  else if($.trim(policyNumber).length == 0)
  {
    $('#policyNumber').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Please enter Policy Number.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }
  else if($.trim(physicianName).length == 0)
  {
    $('#physicianName').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Please enter Physician Name.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }
  else if($.trim(phoneNumber).length == 0)
  {
    $('#phoneNumber').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Please enter Phone Number.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }
  else if($.trim(relationship).length == 0)
  {
    $('#relationship').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Please enter relationship.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }
  else if($.trim(name).length == 0)
  {
    $('#name').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Please enter name.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }
  else if($.trim(address).length == 0)
  {
    $('#address').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Please enter address.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }
  else if($.trim(city).length == 0)
  {
    $('#city').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Please enter city.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }
  else if(states == 0)
  {
    $('#states').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Please select state.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }
  else if(country == 0)
  {
    $('#country').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Please select country.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }
  else if($.trim(homePhone).length == 0)
  {
    $('#homePhone').css('border','1px solid red');
    $('.errorDiv').html(' <strong>Error!</strong> Please enter homePhone.');
    $('.errorDiv').fadeIn().delay(5000).fadeOut();
      
  }
  
  else
  {
      $.ajax({
          url: '<?php echo base_url(); ?>save-registration',
          type: 'POST',
          dataType: 'HTML',
          data :   "firstName="+firstName+"&lastName="+lastName+"&email="+email+"&password="+password+
          "&gender="+gender+"&datepicker1="+datepicker1+"&hospitalPreference="+hospitalPreference+
          "&insuranceCompany="+insuranceCompany+"&policyNumber="+policyNumber+"&physicianName="+physicianName+ 
          "&phoneNumber="+phoneNumber+"&relationship="+relationship+"&name="+name+"&address="+address+"&city="+city+
          "&states="+states+"&country="+country+"&homePhone="+homePhone,
          success: function(res)
            {
              var data = $.parseJSON(res);
                if(data.error == 1 && data.success == 0)
                {
                    $('.errorDiv').html(' <strong>Error!</strong> '+data.errorMsg);
                    $('.errorDiv').fadeIn().delay(5000).fadeOut();
                    
                }
                else if(data.error == 0 && (data.success == 1 || data.success == 2))
                {
                    
                    $('.successDiv').html(' <strong>Success!</strong> '+data.errorMsg);
                    $('#sForm')[0].reset();
                    $('.successDiv').fadeIn().delay(25000).fadeOut();
                }
            },
          error: function(xhr, status, error)
          {
          }
          });

  }
  
}
 function validateEmail(email) 
 {
      var emailPattern         = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (email.search(emailPattern) == -1) // check email format
          {   
           return false;
        }
        else
        {
        return true;  
        }
}				  </script>
			<!-- //Calendar -->

</body>
</html>