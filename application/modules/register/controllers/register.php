<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

error_reporting(1);
class Register extends MX_Controller {


	function __construct()  
	{ 
		parent::__construct();
	 $this->load->model('register_model');	

     $this->load->helper('captcha');
		if($this->session->userdata('client_id'))
		{
		   header('location:'.base_url()."client-area");
		  
		}

	}
	public function resetPasswordSave()
		{
			  $response = array();
			  $data     = array();
			  $data['password']   =   md5($this->input->post('password'));
			  $data['hash']       =   $this->input->post('hash');
			  $resp   = $this->register_model->resetPassword($data);
			  if($resp)
			  {
				$response = ["error"=>0,"errorCode"=>0,"success"=>1];
				echo json_encode($response);
			  }
			  else
			  {
				$response = ["error"=>1,"errorCode"=>1,"success"=>0];
				echo json_encode($response);           
			  }
		}
	
		function changePassword()
		{
			$term   = strip_tags($this->uri->segment(2));
          $resp   = $this->register_model->checkChangePassword($term);

          if($resp)
          {
             $data['hash']   = $term;
            $data['resetPassword']=1;
			$this->load->view('register_view', $data);
          }
          else
          {
 
            header('location:'.base_url());
           
          }

			
		}
	
		function sendPasswordLink()
		{
	

	if ($_POST)
      {
        $resp = array();
        $data = array();

        $data['email']    = $this->input->post('email');
        $data['hash']     = md5(base64_encode($data['email']));

        if (trim($data['email']) == '')
        {
          $resp = ["error"=>1, "errorCode"=>1,"success"=>0];
          echo json_encode($resp);
        }
        else
        {
          $response   =   $this->register_model->checkEmailForPwReset($data['email']);
          
          if ($response == 0)
          {
            $resp = ["error"=>1,"errorCode"=>2,"success"=>0];
            echo json_encode($resp);
          }
          else if($response != 0)
          {
            $res  =  $this->register_model->forgetPassword($data);  
            if ($res != true)
            {
              $resp = ["error"=>1,"errorCode"=>3,"success"=>0];
              echo json_encode($resp);
            }         
            else if($res == true)
            {
              $getData = $this->register_model->getUserDataByEmail($data);

              $name = $getData[0]['first_name'];
              

			   		$emailMessage = '
										
						Click below to reset your password. <br/><br/>
						<a href="'.base_url('password-change/'. $data['hash']).'" >
						<button style="background-color: #008ED6; 
						border: none;
						color: white;
						padding: 10px 25px;
						text-align: center;
						text-decoration: none;
						display: inline-block;
						font-size: 14px;" > Reset Password</button>
						</a>
						<br/><br/>

Regards,<br/>
<br/>
Admin<br/>
<br/>
<b> Ghfriends </b>
						';
              
              $to      = $data['email'];
              $subject = "Reset Password";


				#####
			     $to = $data['email'];
				 $txt = $emailMessage;
				 $headers  = 'MIME-Version: 1.0' . "\r\n";
				 $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				 $headers .= "From: Ghfriends  <noreply@ghfriends.com>" . "\r\n";
				
				  mail($data['email'],$subject,$txt,$headers);
				 //admin@GHF.co

				 ##

              $resp = ["error"=>0,"errorCode"=>0,"success"=>1];
              echo json_encode($resp);
            }
          }
        }

      }
      else
      {
        echo 0;
      }
	}
	function tandc(){
		$this->load->view("tandc");
	}
	function disclaimer(){
		$this->load->view("disclaimer");
	}
	function privacy(){
		$this->load->view("privacy");
	}
	public function index()	
	{	 
	//	$this->load->view('register_view');


	
        
        // Captcha configuration
        $config = array(
            'img_path'      => 'assets/img/',
            'img_url'       => base_url().'assets/img/',
            'img_width'     => '150',
            'img_height'    => 50,
            'word_length'   => 3,
            'font_size'     => 16
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$captcha['word']);
        
        // Send captcha image to view
        $data['captchaImg'] = $captcha['image'];
        
        // Load the view
        $this->load->view('register_view', $data);

	}


	public function refresh()
	{
        // Captcha configuration
        $config = array(
            'img_path'      => 'assets/img/',
            'img_url'       => base_url().'assets/img/',
            'img_width'     => '150',
            'img_height'    => 50,
            'word_length'   => 3,
            'font_size'     => 16
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];





    }



	/*

 public function email()	{				$to = "alii.engineerr@gmail.com";				$subject = "Ghfriends - Verify Your Account";				$txt = "test email checking";				$headers  = 'MIME-Version: 1.0' . "\r\n";				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";				$headers .= "From: Ghfriends  <no-reply@ghfriends.com>" . "\r\n";								var_dump(mail($to,$subject,$txt,$headers));			}	
*/

 public function saveRegistration()	{

	$data['firstname']        =   strip_tags($this->input->post('firstName'));
	$data['lastname']         =   strip_tags($this->input->post('lastName'));
	$data['email']            =   strip_tags($this->input->post('email'));
	$data['password']         =   strip_tags($this->input->post('password'));
	$data['password']         =   md5($this->input->post('password'));
	$data['zipcode']         =   $this->input->post('zipCode');
	/*$data['address']         =   $this->input->post('address');
	$data['city']         =   $this->input->post('city');
	$data['state']         =   $this->input->post('state');
	$data['country']         =   $this->input->post('country'); */
	$data['ip'] = $_SERVER['REMOTE_ADDR'];
	$data['user_location_data']               = json_encode($this->ip_info($_SERVER['REMOTE_ADDR'], "Location"));


        $check_email = $this->register_model->select_by_other_id('email', $data['email'],'tbl_register');

		  if ($check_email != 0)
          {
            $resp = ["error"=>1,"errorMsg"=>"Email already Exist","success"=>0];
            echo json_encode($resp);
            return false;
          }
          else
          {
			 $resp =  $this->register_model->insert($data);
			 
			 if($resp != 0)
			 {
               
				$resp = ["error"=>0,"errorMsg"=>"Your Account has been created successfully.Please check your email address to verify your account.","success"=>2];
                  
			   
			   
			   echo json_encode($resp);
               return false;				 
			 }
             else
			 {
              $resp = ["error"=>1,"errorMsg"=>"Something went wrong","success"=>0];
              echo json_encode($resp);
              return false;	
             }			 
		  }
	}
	function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) 
	{
		$output = NULL;
		if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
			$ip = $_SERVER["REMOTE_ADDR"];
			if ($deep_detect) {
				if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
					$ip = $_SERVER['HTTP_CLIENT_IP'];
			}
		}
		$purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
		$support    = array("country", "countrycode", "state", "region", "city", "location", "address");
		$continents = array(
			"AF" => "Africa",
			"AN" => "Antarctica",
			"AS" => "Asia",
			"EU" => "Europe",
			"OC" => "Australia (Oceania)",
			"NA" => "North America",
			"SA" => "South America"
		);
		if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
			$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
			if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
				switch ($purpose) {
					case "location":
						$output = array(
							"city"           => @$ipdat->geoplugin_city,
							"state"          => @$ipdat->geoplugin_regionName,
							"country"        => @$ipdat->geoplugin_countryName,
							"country_code"   => @$ipdat->geoplugin_countryCode,
							"continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
							"continent_code" => @$ipdat->geoplugin_continentCode
						);
						break;
					case "address":
						$address = array($ipdat->geoplugin_countryName);
						if (@strlen($ipdat->geoplugin_regionName) >= 1)
							$address[] = $ipdat->geoplugin_regionName;
						if (@strlen($ipdat->geoplugin_city) >= 1)
							$address[] = $ipdat->geoplugin_city;
						$output = implode(", ", array_reverse($address));
						break;
					case "city":
						$output = @$ipdat->geoplugin_city;
						break;
					case "state":
						$output = @$ipdat->geoplugin_regionName;
						break;
					case "region":
						$output = @$ipdat->geoplugin_regionName;
						break;
					case "country":
						$output = @$ipdat->geoplugin_countryName;
						break;
					case "countrycode":
						$output = @$ipdat->geoplugin_countryCode;
						break;
				}
			}
		}
		return $output;
	}


	public function userVerification()
	{
		$secret_key	=	$this->uri->segment(2);
		$resp = $this->register_model->userVerification($secret_key);
		if($resp == 1)
		{
		  $data['msg'] = 'Your Email address has been sucessfully Verified. Now you can login';	
           		   header('location:'.base_url()."login");

		  //Redirect to dashboard
     	}
		elseif($resp == 2)
		{
		  $data['msg'] = 'Your Email address had already verified';	
		}
		elseif($resp == 0)
		{
		 $data['msg'] = 'Invalid Verification Code!!';	
		}
		else
		{
          header('location:'.base_url());
		}
		$this->load->view('verify',$data);
		return true;
	}





}
