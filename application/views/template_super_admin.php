<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>::..Admin Panel..::  </title>

  </head>
    <link href="<?php echo base_url(); ?>assets/admin_assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin_assets/bootstrap/dist/css/sweetalert2.css" rel="stylesheet">
	
<script src="<?php echo base_url(); ?>assets/admin_assets/bootstrap/dist/js/sweetalert2.js"></script>
    <link href="<?php echo base_url(); ?>assets/admin_assets/bootstrap/dist/css/font-awesome.min.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="<?php echo base_url(); ?>assets/admin_assets/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>assets/admin_assets/css/custom.min.css" rel="stylesheet">
						<!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/admin_assets/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
<style>
  body
  {
    color: #343b45 ! important;
  }
  .left_col 
  {
    background-color: #343b45! important;
  }
  </style>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="" class="site_title"> <span>Admin</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
              </div>
              <div class="profile_info hide">
                <span>Welcome,</span>
                <h2>Admin</h2>
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- /menu profile quick info -->
            <br />
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                
                <ul class="nav side-menu">
         
				          <li><a><i class="fa fa-files-o"></i> Registered Users <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">  
					           <li><a href="<?php echo base_url(); ?>super-admin/manage-users">Manage Users</a></li>
                    </ul>
                  </li>


                  <li><a><i class="fa fa-files-o"></i> School / Colleges <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>super-admin/manage-schools-colleges">Manage Schools / Colleges</a></li>
                    </ul>
                  </li>


                  <li><a><i class="fa fa-files-o"></i> Degree <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>super-admin/manage-degree">Manage Degree </a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-files-o"></i> Expertise <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>super-admin/manage-expertise">Manage Expertise </a></li>
                    </ul>
                  </li>
                </ul>                
				     </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url(); ?>super-admin/logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo base_url(); ?>assets/admin_assets/build/images/img.jpg" alt="">Admin
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo base_url(); ?>super-admin/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <?php echo $contents ; ?>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">

          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
   <!-- Modal for note -->
  <div class="modal fade" id="orderModal" role="dialog">
    <div class="modal-dialog ">
      <div class="modal-content">
        <div class="modal-body" style="height:150px" >
          <h2>New Order</h2>
          <div>
		  <p id="noteDiv1" >
		  asdasda
		  </p>

                        <div class="col-md-7 spaces">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

							</div>
              
              </div>
              <br/><br/><br/><br/><br/>
        </div>
      </div>
    </div>
  </div>                 

  </body>
    <script src="<?php echo base_url(); ?>assets/admin_assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>assets/admin_assets/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url(); ?>assets/admin_assets/nprogress/nprogress.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url(); ?>assets/admin_assets/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url(); ?>assets/admin_assets/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url(); ?>assets/admin_assets/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin_assets/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="<?php echo base_url(); ?>assets/admin_assets/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin_assets/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin_assets/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="<?php echo base_url(); ?>assets/admin_assets/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="<?php echo base_url(); ?>assets/admin_assets/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url(); ?>assets/admin_assets/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="<?php echo base_url(); ?>assets/admin_assets/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="<?php echo base_url(); ?>assets/admin_assets/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="<?php echo base_url(); ?>assets/admin_assets/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="<?php echo base_url(); ?>assets/admin_assets/starrr/dist/starrr.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url(); ?>assets/admin_assets/js/custom.min.js"></script>
<script>
//setInterval("on();",10000);

function on()
{
	console.log("call");
	$.ajax({
          url: '<?php echo base_url(); ?>super_admin_dashboard/orderChecker',
          type: 'POST',
          dataType: 'HTML',
          data : "data=1",
          success: function(res)
            {
              var data = $.parseJSON(res);
				if(data.isData == 1 && data.data.total > 0)
				{
					$('#noteDiv1').html("You have new "+data.data.total+" order");
					$('#orderModal').modal('show');	
					rn(data.data.order_id);
				}					
            },
          error: function(xhr, status, error)
          {
          }
          });
}
function rn(id)
{
	$.ajax({
          url: '<?php echo base_url(); ?>super_admin_dashboard/read',
          type: 'POST',
          dataType: 'HTML',
          data : "data="+id,
          success: function(res)
            {
            },
          error: function(xhr, status, error)
          {
          }
          });
	
}
</script>
</html>
