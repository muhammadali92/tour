	<?php

class main_manager extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    ##insert function

    public function insert($data, $table) {
        if ($this->db->insert($table, $data)) {
            return true;
        } else {
            return false;
        }
    }
    public function checkExpertise($v) {
        $sql = "SELECT id FROM tbl_general_expertise WHERE expertise_name = '".$v."' ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0)
        {
          $row =  $query->result_array();
		  return $row[0]['id'];
        }
        else
        {
			   		$emailMessage = '
										
								Hi Admin , <br/><br/>
								New expertise Add  name '.$v.'. Please review
								<br/><br/>

		Regards,<br/>
		<br/>
		Admin<br/>
		<br/>
		<b> Ghfriends </b>
						';
              
             // $to      = $data['email'];
              $subject = "New expertise Added . Please Review";


				#####
			     //$to = $data['email'];
				 $txt = $emailMessage;
				 $headers  = 'MIME-Version: 1.0' . "\r\n";
				 $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				 $headers .= "From: Ghfriends  <noreply@ghfriends.com>" . "\r\n";
				
				  mail('alii.engineerr@gmail.com',$subject,$txt,$headers);
             
			 return $this->insert_get_id(array("expertise_name"=>$v,"status"=>0),'tbl_general_expertise'); 
        }
    }
	
 public function postAutoSuggestion($t)
  {
     $sql = "SELECT id,category_name as name,1 as type FROM `tbl_category` WHERE category_name LIKE '%".$t."%'
			UNION SELECT id,CONCAT(first_name, ' ', last_name) as name,2 as type 
			FROM `tbl_register` WHERE first_name LIKE '%".$t."%' OR last_name LIKE '%".$t."%'   OR email LIKE '%".$t."%'   ";
	 $query = $this->db->query($sql);
      if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }

  }
 public function getDegree()
  {
     $query = $this->db->query("select  degree_name AS value,id AS data  FROM tbl_degree ORDER BY degree_name ASC");
      if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }

  }
 public function getExpertise($term)
  {
     $query = $this->db->query("select  expertise_name AS value,id AS data  FROM tbl_general_expertise WHERE  expertise_name LIKE '%".$term."%'    ORDER BY expertise_name ASC");
      if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }

  }

    public function insert_get_id($data, $table) {
        if ($this->db->insert($table, $data)) {
            $insert_id = $this->db->insert_id();
            $this->db->trans_complete();
            return $insert_id;
        } else {
            return false;
        }
    }
    public function listingData($emailArray)
    {
         $query = $this->db->query(" SELECT  l.`listing_name`,l.`description`,l.hash,IFNULL(( SELECT tbl_listing_image.`name` FROM tbl_listing_image WHERE listing_id = l.id AND SUBSTRING_INDEX(tbl_listing_image.name,'.',-1) IN  ('jpg','jpeg','png')   LIMIT 1),'no_img_icon.jpg') AS img_url FROM  `tbl_listing` AS l 
                                            WHERE l.`listing_privacy` = 1 AND 
                                            user_id IN ( SELECT GROUP_CONCAT(id) FROM tbl_user  WHERE company_id = ".$emailArray.")");
        if ($query->num_rows() > 0)
        {
             return $query->result();
        }
        else
        {
            return 0;
        }
    }

    ##update

    public function update($id, $data, $table) {

        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update($table, $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return false;
        } else {
            return true;
        }
    }
   public function updateImages($data)
    {
		$sql = "UPDATE `tbl_register`
				SET 
 				`".$data['col']."` = '".$data['value']."'
				WHERE
				 `id` = '".$data['id']."'";
				$queryData =  $this->db->query($sql);
				if($queryData)
				{
					return 1;
				}
				else
				{
					return 0;
				}
    } 
   public function updatePost($id,$postData,$userId)
    {
		$sql = "UPDATE `tbl_posts`
				SET 
 				`post_desc` = '".$postData."'
				WHERE
				 `id` = '".$id."'   AND user_id =  '".$userId."' ";
				$queryData =  $this->db->query($sql);
				if($queryData)
				{
					return 1;
				}
				else
				{
					return 0;
				}
    } 

    public function update_by_coupon_id($id, $data, $table) {

        $this->db->trans_start();
        $this->db->where('coupon_id', $id);
        $this->db->update($table, $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return false;
        } else {
            return true;
        }
    }
    
    
    public function update_msg_status($resiver_id,$sender_id) {

        $this->db->trans_start();
        $this->db->where('reciever_id', $resiver_id);
        $this->db->where('sender_id', $sender_id);
        $this->db->update('app_messages',array('status' => 1));
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return false;
        } else {
            return true;
        }
    }

    public function update_by_store($id, $data, $table) {

        $this->db->trans_start();
        $this->db->where('store_id', $id);
        $this->db->update($table, $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return false;
        } else {
            return true;
        }
    }

    public function update_by_coupon($id, $data) {

        $this->db->trans_start();
        $this->db->where('product_id', $id);
        $this->db->update("coupons", $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return false;
        } else {
            return true;
        }
    }

    public function update_user_coupon($user_id, $coupon_id, $data) {

        $this->db->trans_start();
        $this->db->where('user_id', $user_id);
        $this->db->where('coupon_id', $coupon_id);
        $this->db->update("coupons", $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return false;
        } else {
            return true;
        }
    }

    public function update_col_name($id, $col_name, $data, $table) {
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->update($table, $data);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return false;
        } else {
            return true;
        }
    }

    ##delete

    public function delete($id, $table) {
        $this->db->where('id', $id);
        if ($this->db->delete($table)) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_by_other_id($col, $col_val, $table) {
        $this->db->where($col, $col_val);
        if ($this->db->delete($table)) {
            return true;
        } else {
            return false;
        }
    }

    ##select all 

    public function select_all($table) {

        $this->db->select("*")
                ->from($table)
                ->order_by("id", "desc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    
    public function select_all_active($table) {

        $this->db->select("*")
                ->from($table)
                ->where('is_active', 1)
                ->order_by("id", "desc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    
    public function select_all_country($table) {
        $this->db->select("*")
                ->from($table)
                ->order_by("name", "ASC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_all_limit($table, $offset, $limit) {

        $this->db->select("*")
                ->from($table)
                ->order_by("id", "desc")
                ->limit($limit, $offset);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    
    public function all_promotion($table, $offset, $limit) {

        $this->db->select("*")
                ->from($table)
                ->where('is_active',1)        
                ->order_by("id", "desc")
                ->limit($limit, $offset);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_all_limit_search($table, $search_key, $search_text, $offset, $limit) {

        $query = $this->db->query("SELECT * from $table where $search_key LIKE '%$search_text%' LIMIT $offset,$limit");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    
    
    public function search_promotion($table, $search_key, $search_text, $offset, $limit) {

        $query = $this->db->query("SELECT * from $table where $search_key LIKE '%$search_text%' AND is_active = 1 LIMIT $offset,$limit");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    ## acending all

    public function select_all_ASC($table) {

        $this->db->select("*")
                ->from($table)
                ->order_by("id", "ASC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_all_row_order($table, $row, $order) {

        $this->db->select("*")
                ->from($table)
                ->order_by($row, $order);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    ##pagination count

    public function record_count($table) {
        return $this->db->count_all($table);
    }

    ##select all for pagination

    public function select_pagination($limit, $start, $table) {

        $this->db->limit($limit, $start);
        $this->db->order_by("id", "desc");
        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    ## select all if status is 1

    public function select_all_status($status_col, $status, $order_by_col, $order_by, $table) {
        $this->db->where($status_col, $status)
                ->order_by($order_by_col, $order_by);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    ## select by id

    public function select_by_id($id, $table) {
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }


    public function select_user_id_is_active($id, $table) {
        $this->db->where('id', $id);
        $this->db->where('is_active', 1);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_by_id_profile($id, $table) {
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return NULL;
        }
    }

    public function where_not_in($user_id, $col, $col_val, $table) {
        $this->db->where_not_in($col, $col_val);
        $this->db->where_not_in('user_id', $user_id);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_by_two_id($id1, $id2, $table) {
        $this->db->where('id', $id);
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function check_device($number, $device) {
        $this->db->where('mobile', $number);
        $this->db->where('device_id', $device);
        $query = $this->db->get('users');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function check_users($number, $device) {
        $this->db->where('mobile', $number);
        $this->db->where('device_id', $device);
        // $this->db->where('is_active',0);
        //$this->db->where('is_deleted', 0);
        $query = $this->db->get('users');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function check_verify($number, $code) {
        $this->db->where('mobile', $number);
        $this->db->where('activation_code', $code);
        $query = $this->db->get('users');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_by_type($type, $table) {
        $this->db->where('type', $type);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    ########select status 1

    public function select_status_on($table) {

        $this->db->select('*')
                ->from($table)
                ->where("status", 1)
                ->order_by("id", "desc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }

    ##count check specifically for delete check either value is present in other table

    public function delete_count($col, $col_val, $table) {
        $this->db->where($col, $col_val);
        $this->db->from($table);
        $count = $this->db->count_all_results();
        return $count;
    }

    public function delete_count_two_columns($col_one, $col_val_one, $col_two, $col_val_two, $table) {

        $this->db->where($col_one, $col_val_one);
        $this->db->where($col_two, $col_val_two);
        $this->db->from($table);
        $count = $this->db->count_all_results();
        return $count;
    }

    ##get all for specic define foreign key id 

    public function select_by_other_id_order($col, $col_val,$order_by_key,$order, $table) {
        $this->db->where($col, $col_val);
        $this->db->order_by($order_by_key, $order);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    
    
    public function select_by_other_id($col, $col_val, $table) {
        $this->db->where($col, $col_val);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    public function getUsersEducation($value) {
        $sql = "SELECT e.description,e.id,e.school_name,e.from_year,e.to_year,d.degree_name FROM `tbl_education` as e 
					INNER JOIN tbl_degree as d
					ON d.id = e.degree_id
					WHERE e.user_id = '".$value."'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    public function getUsersExpertise($value) {
        $sql = "SELECT e.id,ge.expertise_name FROM `tbl_general_expertise` as ge 
					INNER JOIN tbl_expertise as e
					ON ge.id = e.expertise_id
					WHERE e.user_id = '".$value."'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    
    public function store_promotion($col, $col_val, $table) {
        $this->db->where($col, $col_val);
        $this->db->where('is_active',1);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_by_other_id_limit($col, $col_val, $table, $offset, $limit) {
        $this->db->where($col, $col_val);
        $query = $this->db->get($table, $offset, $limit);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    
    public function store_product($col, $col_val, $table, $offset, $limit) {
        $this->db->where($col, $col_val);
        $this->db->where('is_active',1);
        $this->db->where('quantity >', 'no_sold');
        $query = $this->db->get($table, $offset, $limit);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    
    public function store_product_search($col, $col_val, $search_key, $search_text, $table, $offset, $limit) {
        $this->db->where($col, $col_val);
        $this->db->where('is_active',1);
        $this->db->where('quantity >', 'no_sold');
        $this->db->like($search_key, $search_text);
        $query = $this->db->get($table, $offset, $limit);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_by_other_id_search_limit($col, $col_val, $search_key, $search_text, $table, $offset, $limit) {
        $this->db->where($col, $col_val);
        $this->db->like($search_key, $search_text);
        $query = $this->db->get($table, $offset, $limit);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_by_other_id_active($col, $col_val, $table) {
        $this->db->where($col, $col_val);
        $this->db->where('is_active', '1');
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_msg_user($country, $gender) {
        $this->db->where('country_id', $country);
        $this->db->where('gender', $gender);
        $query = $this->db->get('users');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_where_ids_in($ids_array, $table) {
        $query = $this->db->query('SELECT * from ' . $table . ' where id IN (' . implode(",", $ids_array) . ') ');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_limit_status($limit, $table) {

        $this->db->select('*')
                ->from($table)
                ->where("status", 1)
                ->limit($limit)
                ->order_by("id", "desc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }

    public function record_count_join($table1, $table2, $col1, $col2) {

        $this->db->select('count(*) as total')
                ->from($table1)
                ->join($table2, $table1 . "." . $col1 . "=" . $table2 . "." . $col2, 'inner');
        $query = $this->db->get();
        $count = $query->row_array();
        return $count['total'];
    }

    public function delete_user_coupon($user_id, $coupon_id) {
        $this->db->where('user_id', $user_id);
        $this->db->where('coupon_id', $coupon_id);
        if ($this->db->delete('user_coupons')) {
            return true;
        } else {
            return false;
        }
    }

    public function is_user_coupon($user_id, $coupon_id) {
        $query = $this->db->query('SELECT * from user_coupons where user_id = ' . $user_id . ' AND coupon_id = ' . $coupon_id . ' AND is_redeemed = 0');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function get_this_by_other_id($term, $other_id, $other_value, $table) {
        $query = $this->db->query("SELECT $term FROM $table WHERE " . $other_id . " = $other_value AND is_active = 1");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
	public function getTravellingPurpose()
	{
	   $query = $this->db->query("SELECT id,title FROM `travelling_purpose` WHERE is_active = 1");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
	}
	public function getAllCountry()
	{
	   $query = $this->db->query("SELECT id,name FROM `countries`");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
	}
	public function addVisaRequirement($importantInformation,$admin_id,$holderCountryData,$destinationCountryData,$visaTypeTmp,$visaRequiredTmp,$purposeId,$vat)
	{
			 $sql = "INSERT INTO `visa_applications_requirements`
            (
             `user_id`,
			 `important_info`,
             `visa_type`,
             `passport_holder_country`,
             `destination_country`,
             `purpose_id`,
             `offer_visa`,
             `visa_required`,
             `vat`
             )
		VALUES (
				'".$admin_id."',
				'".$importantInformation."',
				'".$visaTypeTmp."',
				'".$holderCountryData."',
				'".$destinationCountryData."',
				'".$purposeId."',
				'1',
                '".$visaRequiredTmp."',
                '".$vat."'
				)";
		   
		$query =  $this->db->query($sql);
		if($query)
		{

			$insert_id  = $this->db->insert_id();
			return $insert_id;
		}
		else
		{
		  return 0;
		}
	}
	
	public function addFees($visa_app_req_id,$type,$processingTime,$handlingFee,$consularFee,$service,$total,$cost)
	{
			 $sql = "	INSERT INTO `fees`
		    (
		     `type`,
		     `service`,
		     `consular_fee`,
		     `handling_fee`,
		     `total_fee`,
		     `processing_time`,
		     `visa_app_req_id`,
             `cost`
		     )
			VALUES (
				'".$type."',
				'".$service."',
				'".$consularFee."',
				'".$handlingFee."',
				'".$total."',
				'".$processingTime."',
				'".$visa_app_req_id."',
                '".$cost."')
				";
				   
		$query =  $this->db->query($sql);
		if($query)
		{

			$insert_id  = $this->db->insert_id();
			return $insert_id;
		}
		else
		{
		  return 0;
		}
	}
	
    public function addCustomRequirement($visa_app_req_id,$requirementName,$requirementDetail,$requirementImg)
	{
			 $sql = "INSERT INTO `visa_requirement_custom`
            (
             `visa_app_req_id`,
             `requirement_name`,
             `requirement_details`,
             `image_icon`)
						 
			VALUES (
					'".$visa_app_req_id."',
					'".$requirementName."',
					'".$requirementDetail."',
                    '".$requirementImg."')";
				   
		$query =  $this->db->query($sql);
		if($query)
		{

			$insert_id  = $this->db->insert_id();
			return $insert_id;
		}
		else
		{
		  return 0;
		}
	}
   public function addAdditionalFees($title,$price,$id)
	{
			 $sql = "INSERT INTO `additional_fees`
            (
             `title`,
             `price`,
             `visa_app_req_id`)
			VALUES (
				'".$title."',
				'".$price."',
				'".$id."'
					)";
				   
		$query =  $this->db->query($sql);
		if($query)
		{

			$insert_id  = $this->db->insert_id();
			return $insert_id;
		}
		else
		{
		  return 0;
		}
	}
	public function addRequiredDocuments($fileName,$fileUrl,$id)
	{
			 $sql = "INSERT INTO `required_documents`
					(
					 `file_name`,
					 `file_url`,
					 `visa_app_req_id`
					 
					 
					 )
					VALUES (
							'".$fileName."',
							'".$fileUrl."',
							'".$id."'
							);";
				   
		$query =  $this->db->query($sql);
		if($query)
		{

			$insert_id  = $this->db->insert_id();
			return $insert_id;
		}
		else
		{
		  return 0;
		}
	}
		### taimur model
		 public function getUserDatas($id)
  {
     $query = $this->db->query("select * from users where id = '".$id."'");
      if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }

  }
  public function userAuth($data)
  {
     // $query = $this->db->query("select id from users where email = '".$data['email']."' AND password = '".md5($data['password'])."'");
        $query = $this->db->query("select id from users where email = '".$data['email']."' AND password = '".md5($data['password'])."'  ");

        if ($query->num_rows() > 0) {

            $checkBlockQuery = $this->db->query("select id from users where email = '".$data['email']."' AND password = '".md5($data['password'])."' AND status = '1' ");
                if ($checkBlockQuery->num_rows() > 0) 
                {
                    $row = $query->result_array();
             
                    $this->session->set_userdata('user_id',$row[0]['id']);
                    return 1;
                }
                else
                {
                    return 3;
                }
            
        } 
        else
         {
            return 0;
         }

  }

  public function getCountries()
  {
     $query = $this->db->query("select Code,name FROM tbl_country ORDER BY Name ASC");
      if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }

  }

  public function getStatesById($id)
  {
     $query = $this->db->query("select id,name FROM tbl_city WHERE CountryCode = '".$id."'  ORDER BY name ASC");
      if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }

  }

  public function getStates()
  {
     $query = $this->db->query("select id,name FROM tbl_city ORDER BY name ASC");
      if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }

  }
  public function get_faq($table) {

        $this->db->select('*')
                ->from($table)
                ->where("is_active", 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }
    public function getEmbassyData($startParam = 'a')
    {
        $query = $this->db->query(" SELECT c.`name` , i.`address`,i.`phone`,i.`fax`,i.`email`,i.`website`,i.`consular_email`,i.`consular_fax`,i.`opening_hours`
                                    ,i.`consular_opening_hour`,i.`consular_phone`  FROM   `tbl_embassy_info` AS i
                                     INNER JOIN `countries` AS c
                                     ON c.`id` = i.`country_id`
                                     WHERE i.`country_start` = '".$startParam."' ORDER BY c.`name` ASC");
                                      if ($query->num_rows() > 0) 
                                      {
                                            return $query->result_array();
                                      }
                                       else
                                        {
                                            return 0;
                                        }
    }

function checkUserRegistration($hash)
{
    $query = $this->db->query("SELECT id,first_name,last_name FROM `tbl_user` WHERE hash = '".$hash."' AND status = 0");
                              
    if ($query->num_rows() > 0) 
    {
        $update = "UPDATE tbl_user SET hash = '',status = 1 WHERE hash = '".$hash."'";
        $q      =  $this->db->query($update); 
        if($q)
        {
                return $query->result_array();
        }
        else
        {
                return 0;
        }
    }
    else
    {
        return 0;
    }
}

  public function getDataByPk($table,$data,$id)
  {
     $query = $this->db->query("select ".$data."  FROM ".$table." WHERE id = '".$id."' ");
      if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }

  }
  public function getDataByFK($table,$data,$id,$fk)
  {

	 $query = $this->db->query("select ".$data."  FROM ".$table." WHERE ".$fk." = '".$id."' ");
      if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }

  }

  // UBAID

  public function getComapanyName($params=array())
  {
     $sql = "select id,company_name,country_id FROM tbl_company";
     $query = $this->db->query($sql);
      if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }

  }

   public function existComapanyName($id,$getCompany)
  {
     $sql = "select id,company_name,country_id FROM tbl_company WHERE country_id = '".$id."' AND company_name = '".$getCompany."'";
     $query = $this->db->query($sql);
      if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }

  }

     public function existComapanyNameConutryId($id,$company)
      {
         $sql = "select id,company_name,country_id FROM tbl_company WHERE country_id = '".$id."' AND company_name = '".$company."'";
         $query = $this->db->query($sql);
          if ($query->num_rows() > 0) {
               return $query->result_array();
            } else {
                return 0;
            }

      }

public function getUserProfilePic($id)
{
    $sql = "SELECT u.profile_pic,c.`company_name`,first_name,last_name FROM tbl_user u
            INNER JOIN tbl_company c ON c.id = u.company_id
            where u.id = ".$id." ";
    $query = $this->db->query($sql);
    if($query->num_rows() > 0)
    {
        $row = $query->result_array();
        return $row;
    }
    else
    {
        return 0;
    }
}

public function select_all_order_asc($table) {

        $this->db->select("*")
                ->from($table)
                ->order_by("id", "asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    ########################## 2-MARCH-2017 TAIMUR #################################

public function views($data)
{
    
    if($data['type'] == 1)
    {
        $view_type_id = $data['listing_id'][0]['id'];
    }
    else
    {
        $view_type_id = $data['type_id'];
    }
     $sql = "INSERT INTO `tbl_views`
            (
             `type`,
             `viewer_id`,
             `view_type_id`
             )
        VALUES (
                ".$data['type'].",
                ".$data['user_id'].",
                ".$view_type_id."
                )";

    $query = $this->db->query($sql);
    if($query)
    {
        return true;
    }
    else
    {
        return false;
    }
}

public function getListingID($data)
{
    $sql = "SELECT id from tbl_listing where hash  = ".$data['type_id']." ";
    $query = $this->db->query($sql);
    if($query->num_rows() > 0)
    {
        $row = $query->result_array();
        return $row;
    }
    else
    {
        return 0;
    }
}

public function getEmailId($id)
{
    $sql = "SELECT first_name,last_name,email_address from tbl_user WHERE id = ".$id." ";

    $query = $this->db->query($sql);

    if ($query->num_rows() > 0)
    {
        return $query->result_array(); 
    }
    else
    {
        return 0;
    }
}

public function companyURL($id)
{
    $sql = "SELECT company_id from tbl_user where id = ".$id." ";
    $query = $this->db->query($sql);

    if ($query->num_rows() > 0)
    {
        $row = $query->result_array(); 
        $company = "SELECT id,company_name from tbl_company where id = ".$row[0]['company_id']." ";
        $cQuery = $this->db->query($company);
        if($cQuery->num_rows() > 0)
        {
            $rowData = $cQuery->result_array(); 
            $company_id = $rowData[0]['id'];

            $company_idCount  =  strlen( $company_id);

            $hash             =  md5(base64_encode($company_id));
            $companyName      =  strtolower( $rowData[0]['company_name']).'-'.$company_idCount;
            $companynameArray =  str_replace(" ","-",$companyName);
            $companyURL =  base_url().'company/'.$company_id.$hash.'/'.$companynameArray; 
            return $companyURL;
        }
    }
    else
    {
        return 0;
    }
}

public function profileURL($id)
{
    $sql = "SELECT id,first_name,last_name,email_address from tbl_user where id = ".$id." ";
    $query = $this->db->query($sql);

    if ($query->num_rows() > 0)
    {
        $row = $query->result_array();

        $idCount = strlen($row[0]['id']);
        $hash     =  md5(base64_encode($row[0]['email_address']));
        $fullName     =  strtolower($row[0]['first_name']).'-'.strtolower($row[0]['last_name']).'-'.$idCount;
        $profileURL  = base_url().'profile/'.$row[0]['id'].$hash.'/'.$fullName;
        return $profileURL;
    }
    else
    {
        return 0;
    }
}

//// ACCESS RIGHTS ////

public function getAccessRights($id)
{
    $sql = "SELECT ar.* FROM tbl_access_rights ar 
            INNER JOIN tbl_user u ON u.id = ar.`user_id`
            WHERE ar.`user_id` = ".$id." ";

    $query = $this->db->query($sql);

    if ($query->num_rows() > 0)
    {
        return $query->result_array(); 
    }
    else
    {
        return 0;
    }
}

public function getuserId($id)
{   
     $sql = "SELECT company_id,parent_id FROM `tbl_user` WHERE id = ".$id." ";
    $query = $this->db->query($sql);
    if ($query->num_rows() > 0)
    {
        $row  = $query->result_array();
        return  $row;
    }
    else
    {
        return 0;
    }
}

public function getCompanyData($id)
{   
     $sql = "SELECT expiry_date FROM `tbl_company` WHERE id = ".$id." ";
    $query = $this->db->query($sql);
    if ($query->num_rows() > 0)
    {
        $row  = $query->result_array();
        return  $row;
    }
    else
    {
        return 0;
    }
}

public function getLineAdminData($id)
{   
     $sql = "SELECT id,first_name,last_name FROM `tbl_user` WHERE id = ".$id." ";
    $query = $this->db->query($sql);
    if ($query->num_rows() > 0)
    {
        $row  = $query->result_array();
        return  $row;
    }
    else
    {
        return 0;
    }
}

public function companyListingAddons($id)
{
    $sql = "SELECT company_id from tbl_user where id = ".$id." ";
    $query = $this->db->query($sql);

    if ($query->num_rows() > 0)
    {
        $row = $query->result_array();

        $sql = "SELECT SUM(a.addon_count) AS listing FROM tbl_addons a
        INNER JOIN tbl_user u ON u.id = a.user_id
        WHERE u.company_id = ".$row[0]['company_id']." AND a.addon_type = 2";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0)
        {
            $row  = $query->result_array();
            return  $row;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return 0;
    }
}

public function countListing($id)
{
    $sql = "SELECT company_id from tbl_user where id = ".$id." ";
    $query = $this->db->query($sql);

    if ($query->num_rows() > 0)
    {
        $row = $query->result_array();

        $sql = "SELECT COUNT(l.id) AS totalListing FROM tbl_listing l
        INNER JOIN tbl_user u ON u.id = l.user_id
        where u.company_id = ".$row[0]['company_id']." ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0)
        {
          $row= $query->result_array();
          return $row;
        }
        else
        {
          return 0;
        }
    }
    else
    {
        return 0;
    }
}

 public function getCitiesById($id,$cityAlpha)
  {
     $query = $this->db->query("select  name AS value,id AS data  FROM tbl_city WHERE CountryCode = '".$id."' AND name LIKE '%".$cityAlpha."%'  GROUP BY name  ORDER BY name ASC");
      if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }

  }

  public function citiesID($id)
    {
        $sql = "SELECT Name FROM tbl_city WHERE id = ".$id." ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return 0;
        }
    }

  public function getPaymentChk($id)
  {  

    $sql = "SELECT company_id from tbl_user where id = ".$id." ";
    $query = $this->db->query($sql);

    if ($query->num_rows() > 0)
    {
        $row = $query->result_array();

        $sql = "SELECT id FROM tbl_payments WHERE company_id = ".$row[0]['company_id']." ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0)
        {
          return 1;
        }
        else
        {
          return 0;
        }
    }
    else
    {
        return 0;
    }

  }
  function nicetime($date)
  {
    if(empty($date)) {
        return "No date provided";
    }
 
    $periods         = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
    $lengths         = array("60","60","24","7","4.35","12","10");
 
    $now             = time();
    $unix_date         = strtotime($date);
 
       // check validity of date
    if(empty($unix_date)) {    
        return "Bad date";
    }
 
    // is it future date or past date
    if($now > $unix_date) {    
        $difference     = $now - $unix_date;
        $tense         = "ago";
 
    } else {
        $difference     = $unix_date - $now;
        $tense         = "from now";
    }
 
    for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
        $difference /= $lengths[$j];
    }
 
    $difference = round($difference);
 
    if($difference != 1) {
        $periods[$j].= "s";
    }
 
    return "$difference $periods[$j] {$tense}";
  }
  
  public function getPost($category,$s)
  {

    $where = "";
	  $l = 0 ;
	  if($category != 0 && $category !=  'user'){
		  $where = " WHERE p.category_id = ".$category;
		  $l = 1;
	  }
	  if($category ==  'user'){
		  $where = " WHERE p.user_id = ".$s;
	  }

	  if($s != '0' && $category !=  'user'){

		 $where .=  ($l == 0) ? " WHERE " : " AND ";
			   
		  $where .= "  p.post_desc   LIKE '%".$s."%' ";
	  }  
	  $sql = "SELECT p.*,p.created_date,u.profile_picture,u.first_name,u.last_name,c.category_name from tbl_posts as p 
		INNER JOIN tbl_register as u
		on p.user_id = u.id
		INNER JOIN tbl_category as c
		on p.category_id = c.id ".$where. " ORDER BY p.id DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0)
        {
			$row =  $query->result_array();
			foreach($row as $key => $rowData){
				if($rowData['user_id'] ==  $this->session->userdata('client_id')){
					$rowData['owner'] = 1;
				}else{
					$rowData['owner'] = 0;
					
				}
				$rowData['time'] = $this->nicetime($rowData['created_date']);
				$postData[] =	$rowData;
			}
			return $postData;

        }
        else
        {
          return 0;
        }
		
  }  public function searchPost($key)
  {
	  $where = "";
	  if($category != 0){
		  $where = " WHERE p.category_id = ".$category;
	  }
	  $sql = "SELECT p.* from tbl_posts as p 
			 WHERe 	";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0)
        {
			$row =  $query->result_array();
			foreach($row as $key => $rowData){
				if($rowData['user_id'] ==  $this->session->userdata('client_id')){
					$rowData['owner'] = 1;
				}else{
					$rowData['owner'] = 0;
					
				}
				$rowData['time'] = $this->nicetime($rowData['created_date']);
				$postData[] =	$rowData;
			}
			return $postData;

        }
        else
        {
          return 0;
        }
		
  }
  
}

?>