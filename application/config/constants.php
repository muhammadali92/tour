<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');




$protocol = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ) ? 'https' : 'http';

define('DOCUMENT_ROOT', 		$_SERVER['DOCUMENT_ROOT']);
define('SITE_URL', 				$protocol.'://www.autokonnekt.com/'); 
define('IMAGES_PATH', 			SITE_URL.'images/');
define('TEMPLATE_IMAGES', 			SITE_URL.'assets/templates_screens/');

define('__date_time', 	date('Y-m-d H:i:s'));


define('SMTPHOST','103.84.175.34');
define('SMTPUSERMASTER','master');
define('SMTPUSERSLAVE','slave');

define('SMTPPASS','*Rashid2017!');
define('SMTPPORT',3973);
define('BOUNCEEMAIL','postman@autokonnekt.com');
define('FROMEMAIL','postman@autokonnekt.com');


  
   
  











/* End of file constants.php */
/* Location: ./application/config/constants.php */