<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sg_Common_Model extends CI_Model
{	
	public $_table_owner;
	public $_table_salons;
	public $_table_owner_packages;
	public $_table_salon_employee;
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_salon_id($owner_id=0)
	{		
		$this->db->where('salon_owner', $owner_id);
		$this->db->where('salon_status', "1");
		if($data = $this->db->get($this->_table_salons)->row())
		{
			$salon_id	=	$data->id;	
			return $salon_id;
		} 
		else 
		{
			$salon_id	=	0;
			return $salon_id;
		}
	}

	public function get_date_owner($owner_id=0)
	{
		$this->db->where('user_id', $owner_id);
		$this->db->where('o_status', "1");
		if($data = $this->db->get('tbl_user')->row())
		{
			$created	=	$data->created;	
			return $created;
		} 
		else 
		{
			$created	=	'';
			return $created;
		}
	}

	public function get_package_info($owner_id=0,$salon_id=0)
	{ 
		/*$sql="SELECT pkg.pkg_name,opkg.pkg_id FROM ".$this->_table_owner_packages." opkg join sg_packages pkg on opkg.pkg_id=pkg.pkg_id WHERE opkg.salon_id=".$salon_id." AND opkg.owner_id=".$owner_id." AND opkg.status=1 AND pkg.pkg_status=1 AND opkg.is_package_cancelled=0";*/
		$sql="SELECT pkg.pkg_name,opkg.pkg_id,pkg.pkg_price FROM ".$this->_table_owner_packages." opkg join sg_packages pkg on opkg.pkg_id=pkg.pkg_id WHERE opkg.salon_id=".$salon_id." AND opkg.owner_id=".$owner_id." AND opkg.status=1 AND pkg.pkg_status=1";
		$query = $this->db->query($sql);
		if($query)
		{
			return $query->result();
		}
		else
		{
			return array();
		}
	}
	
	public function get_status_owner($owner_id=0)
	{
		$this->db->where('id', $owner_id);
		$this->db->where('o_status', "1");
		if($data = $this->db->get($this->_table_owner)->row())
		{
			$status	=	$data->o_status;	
			return $status;
		} 
		else 
		{
			$status	=	0;
			return $status;
		}
	}
	// get package of the owner // hassan
	public function get_pkg_owner($owner_id=0)
	{
		$this->db->where('id', $owner_id);
		$this->db->where('o_status', "1");
		if($data = $this->db->get($this->_table_owner)->row())
		{
			$owner_pkg	=	$data->sg_packages;	
			return $owner_pkg;
		} 
		else 
		{
			$owner_pkg	=	0;
			return $owner_pkg;
		}
	}	
	
	public function get_salon_employee($owner_id=0,$emp_id=0)
	{
		if($emp_id>0)
		{
			$sql = "select * from ".$this->_table_salon_employee."
			where owner_id=".$owner_id." AND emp_id=".$emp_id." AND emp_status = '1'";
		}
		else{
			$sql = "select * from ".$this->_table_salon_employee."
			where owner_id=".$owner_id." AND emp_status = '1' ORDER BY emp_id DESC";
		}
		
		$query=$this->db->query($sql);
		if($query)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	// hassan changes for employee access level 07-10-2015-09-15
	 public function giveEmployeeAccessRights($group_id,$option_name,$var_prefix)
	{
	  $sql = "SELECT g_a.can_add,g_a.can_view,g_a.can_update,g_a.can_delete,a_opt.option_name FROM 
	  sg_group_access AS g_a 
	  INNER JOIN sg_access_options AS a_opt
	  ON g_a.option_id = a_opt.option_id
	  WHERE group_id = '".$group_id."' AND option_name = '".$option_name."'";
	  $query =  $this->db->query($sql);
	  $results = $query->result();
	  if( !empty($results) )
	  {
		  $this->session->set_userdata(array(
			  "".$var_prefix."_can_view"=>$results[0]->can_view,
			  "".$var_prefix."_can_add"=>$results[0]->can_add,
			  "".$var_prefix."_can_update"=>$results[0]->can_update,
			  "".$var_prefix."_can_delete"=>$results[0]->can_delete,
			  "status"=>TRUE
		  ));
	  }
	  else
	  {
		  $this->session->set_userdata(array(
			  "".$var_prefix."_can_view"=>0,
			  "".$var_prefix."_can_add"=>0,
			  "".$var_prefix."_can_update"=>0,
			  "".$var_prefix."_can_delete"=>0,
			  "status"=>TRUE
		  ));
	  }
	}	
	
}
?>